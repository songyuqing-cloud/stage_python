from scipy import signal
import numpy as np
import matplotlib.pyplot as plt

for loop in range(10,1000,100):
    sig = np.repeat([0., 1., 0.], loop)
    win = np.repeat([0.,1., 0.], loop)
    print(sig)
    filtered = signal.convolve(sig, win, mode='same')
    fig, (ax_orig, ax_win, ax_filt,numpy_convolve) = plt.subplots(4, 1, sharex=True)
    ax_orig.plot(sig)
    ax_orig.set_title('Original pulse')
    ax_orig.margins(0, 0.1)
    ax_win.plot(win)
    ax_win.set_title('Filter impulse response')
    ax_win.margins(0, 0.1)
    ax_filt.plot(filtered)
    ax_filt.set_title('Filtered signal')
    ax_filt.margins(0, 0.1)
    numpy_convolve.set_title('numpy_convolve')
    numpy_convolve.plot(np.convolve(sig, win,mode='same')/sum(win))
    fig.tight_layout()
    fig.show()
